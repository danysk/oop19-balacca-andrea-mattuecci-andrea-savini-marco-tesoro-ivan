package generali.testController;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import org.junit.jupiter.api.Test;

import application.C.*;
import application.M.*;


class WarehouseUtilityImplTest {
	
	private ArrayList<Typology> typology = new ArrayList<>();
	private ArrayList<String> temp = new ArrayList<>();
	WarehouseAddUtility wAdd = new WarehouseAddUtilityImpl();	
	WarehouseModifyUtility wMod = new WarehouseModifyUtilityImpl();	
	WarehouseDeleteUtility wDel =  new WarehouseDeleteUtilityImpl();
	Catena catena =  new Catena();

	@Test
	void testAddTipologia() {		
		String idTipologia = "Pesce";
		String idInfor = "Colore";
		String valoreInfo = "Marrone";
		typology.add(wAdd.addNewTipologia(catena, idTipologia, idInfor, valoreInfo));
		catena.aggiungiAllInventario(typology);
		Tipologia t = (Tipologia)catena.ottieniDallInventario(idTipologia).get();
		assertEquals(t, (Tipologia)catena.ottieniDallInventario(idTipologia).get());		
	}
	
	@Test
	void testAddProdotto() {
		String idTipo = "Pesce";
		String idProdotto = "Branzino";
		String idInfor = "Provenienza";
		String valoreInfo = "Italia";
		typology.add(wAdd.addNewTipologia(catena, idTipo, "", ""));
		typology.add(wAdd.addNewProdotto(catena, idProdotto, idTipo, "", "", idInfor, valoreInfo));
		catena.aggiungiAllInventario(typology);
		Prodotto p = (Prodotto)catena.ottieniDallInventario(idProdotto).get();
		assertEquals(true, catena.getInventario().contains(p));		
	}
	
	@Test
	void testAddProdConcreto() {
		String idTipo = "Pesce";
		String id = "Branzino";
		String idProd = "Branzino200";
		typology.add(wAdd.addNewTipologia(catena, idTipo, "", ""));
		catena.aggiungiAllInventario(typology);
		typology.add(wAdd.addNewProdConcreto(catena, idProd, id, "", "", "", ""));
		assertEquals(false, catena.ottieniDallInventario(idProd).isPresent());
	}
	
	@Test
	void testModifyInfoProdotto() {
		String idTipo = "Pesce";
		String idProd = "Branzino";
		String key = "colore";
		String value = "rosso";
		String newValue = "nero";
		temp.add(key);		
		wAdd.addNewTipologia(catena, idTipo, "", "");
		wAdd.addNewProdotto(catena, idProd, idTipo, "", "", key, value);		
		wMod.modifyProdotto(catena, idProd, "", "",key, newValue);
		Prodotto p = (Prodotto)catena.ottieniDallInventario(idProd).get();
		assertEquals(newValue, p.getInfo().get(key));
	}
	
	@Test
	void testModifyScarto() {
		String idTipo = "Pesce";
		String idProd = "Branzino";
		String key = "testa";
		String value = "10";
		String newValue = "20";
		wAdd.addNewTipologia(catena, idTipo, "", "");
		wAdd.addNewProdotto(catena, idProd, idTipo, key, value, "", "");
		wMod.modifyProdotto(catena, idProd, key, newValue, " ", " ");
		Prodotto p = (Prodotto)catena.ottieniDallInventario(idProd).get();
		assertEquals(Float.parseFloat(newValue), p.getScarti().stream().filter(e->e.getID().equals(key)).findFirst().get().getQuantita().floatValue());
	}
	
	@SuppressWarnings("unlikely-arg-type")
	@Test
	void testRemove() {
		ArrayList<Scarto> scarti = new ArrayList<>();
		String idTipo = "Pesce";
		String idProd = "Branzino";
		String key = "colore";
		String value = "rosso";
		String idScarto = "testa";
		String valScarto = "10";
		wAdd.addNewTipologia(catena, idTipo, key, value);
		wAdd.addNewProdotto(catena, idProd, idTipo, idScarto, valScarto, "", "");
		wDel.deleteTipo(catena, idTipo, key);
		wDel.deleteProdotto(catena, idProd, key, idScarto);
		Tipologia t = (Tipologia)catena.ottieniDallInventario(idTipo).get();
		Prodotto p =(Prodotto)catena.ottieniDallInventario(idProd).get();
		scarti.add(new Scarto(idScarto));
		assertEquals(false, t.getInfo().containsKey(key));
		assertEquals(false, p.getScarti().contains(scarti));
		
	}	
}
