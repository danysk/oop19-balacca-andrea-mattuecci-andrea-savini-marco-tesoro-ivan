package generali.testModel;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import application.M.Catena;
import application.M.Fornitore;
import application.M.ProdConcreto;
import application.M.ProdFornito;
import application.M.Prodotto;
import application.M.Scarto;
import application.M.Tipologia;
import application.M.Typology;

class FornitoreTest {

	@Test
	void test() {
		HashMap<String, String> mappa = new HashMap<String, String>();
        HashMap<String, String> mappa2 = new HashMap<String, String>();
        ArrayList<String> lista = new ArrayList<String>();
        ArrayList<Scarto> listas = new ArrayList<>();
        ArrayList<String> lista2 = new ArrayList<String>(); //strutture d'appoggio
        
        Catena catena = new Catena();
        Tipologia pesce = new Tipologia("Pesce"); // creo il pesce
        Prodotto branzino = new Prodotto("Branzino", pesce); // creo il branzino dicendogli che è di tipo pesce
        ProdConcreto branzino200g = new ProdConcreto("Branzino200g", branzino); //...
        ProdFornito branzino200gGeloService = new ProdFornito("Branzino200gGeloService", branzino200g);
        
        listas.add(new Scarto("ScartoA")); //creo uno scarto (si autoinizializza a 0)
        
        branzino.aggiungiScarti(listas); //lo aggiungo a branzino
        
        Scarto scartoa = new Scarto("ScartoA"); //creo un ALTRO scarto con lo stesso nome
        scartoa.setQuantita((float) 25);
        scartoa.setPercentuale(true); // lo definisco
        
        listas.clear();
        listas.add(scartoa);
        branzino200gGeloService.modificaScarti(listas); //così modifico (ovviamente devo passare un oggetto scarto diverso da quello precedente altrimenti mantiene i riferimenti)
        
        //assertEquals("25.0",branzino200gGeloService.ottieniScartiTotali().get(0).getQuantita().toString());
        //dato che è stato modificato il valore, viene restituito quello a livello base ed il lookup non serve
        
        lista.clear();
        lista.add("ScartoA");
        
        //assertEquals("0.0",branzino200g.ottieniScartiTotali().get(0).getQuantita().toString());
        //col lookup che parte da branzino200g non ho il valore definito più in basso
        
        listas.clear();
        listas.add(new Scarto("ScartoB"));
        listas.get(0).setQuantita((float) 25);
        listas.get(0).setPercentuale(true); // lo definisco
        
        branzino200g.aggiungiScarti(listas); //aggiungo il nuovo scarto a branzino 200g
        
        branzino200gGeloService.setPrezzo((float)4);
        branzino200gGeloService.setValoreAssoluto((float)0.2);
        branzino200gGeloService.setIDFornitore("Gelo");
        
        //System.out.println(branzino200gGeloService.getPrezzoEffettivo().toString() + " -- " + branzino200gGeloService.getValoreNetto());
        
        ProdFornito branzino200gBestForn = new ProdFornito("Branzino200gBestForn", branzino200g);
        branzino200gBestForn.setPrezzo((float)4);
        branzino200gBestForn.setValoreAssoluto((float)0.2);
        branzino200gBestForn.setIDFornitore("Best");
        
        catena.aggiungiUnFornitore(new Fornitore("Best"));
        catena.aggiungiUnFornitore(new Fornitore("Gelo"));
        
        ArrayList<Typology> ll = new ArrayList<Typology>(); //struttura d'appoggio
        ll.add(pesce); ll.add(branzino); ll.add(branzino200g); ll.add(branzino200gBestForn); ll.add(branzino200gGeloService);
        catena.aggiungiAllInventario(ll); //aggiungo all'inventario le typology
        
        //assertEquals("5.0 -- 0.15",branzino200gBestForn.getPrezzoEffettivo().toString() + " -- " + branzino200gBestForn.getValoreNetto());
        //mi aspetto quindi un prezzo effettivo più grande del 25% e un velore netto effettivo più piccolo del 25%
        
        assertEquals("migliori prodotti gelo: - []","migliori prodotti gelo: - " + catena.ottieniUnFornitore("Gelo").get().getMiglioriProdotti(catena).keySet().toString());
        assertEquals("migliori prodotti best: - [Branzino200g]","migliori prodotti best: - " + catena.ottieniUnFornitore("Best").get().getMiglioriProdotti(catena).keySet().toString());
        
        assertEquals("miglior fornitore di branzino 200g: - Best","miglior fornitore di branzino 200g: - " + branzino200g.getIDMigliorFornitore(catena));
	}

}
