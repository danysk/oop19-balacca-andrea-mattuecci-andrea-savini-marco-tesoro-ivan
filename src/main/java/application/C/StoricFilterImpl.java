package application.C;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import application.M.Catena;


/**
 * @author marco
 *
 */
public class StoricFilterImpl implements StoricFilter {

    @Override
	public Map<Date, Map<String, Float>> searchload (final Catena catena, final String idhotel, final String idDisp, final Date dateStart, final Date dateEnd){
		final Map<Date, Map<String, Float>> tmp = new TreeMap<>();
		if (catena.ottieniUnAlbergo(idhotel).isPresent()
				&& catena.ottieniUnAlbergo(idhotel).get().ottieniUnaDispensa(idDisp).isPresent()) {
			for (final var x : catena.ottieniUnAlbergo(idhotel).get().ottieniUnaDispensa(idDisp).get().getCarichi().keySet()) {			
				if (x.after(dateStart) & x.before(dateEnd)) {
					tmp.put(x, catena.ottieniUnAlbergo(idhotel).get().ottieniUnaDispensa(idDisp).get().getCarichi().get(x)); 
				}
			}
		}
		return tmp;
		
	}

    @Override
	public Map<Date, Map<String, Float>> searchdump(final Catena catena, final String idhotel, final String idDisp, final Date dateStart, final Date dateEnd) {
		final Map<Date, Map<String, Float>> tmp = new TreeMap<>();
		if (catena.ottieniUnAlbergo(idhotel).isPresent()
				&& catena.ottieniUnAlbergo(idhotel).get().ottieniUnaDispensa(idDisp).isPresent()) {
			for (final var x : catena.ottieniUnAlbergo(idhotel).get().ottieniUnaDispensa(idDisp).get().getScarichi().keySet()) {			
				if (x.after(dateStart) & x.before(dateEnd)) {
					tmp.put(x, catena.ottieniUnAlbergo(idhotel).get().ottieniUnaDispensa(idDisp).get().getScarichi().get(x)); 
				}
			}
		}
		return tmp;
	}

}
