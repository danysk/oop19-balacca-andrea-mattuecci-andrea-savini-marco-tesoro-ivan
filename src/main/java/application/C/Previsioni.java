package application.C;

import java.util.Date;
import java.util.HashMap;
import java.util.NavigableMap;

import application.C.Consumi.*;

public interface Previsioni {
    
	/**
     * @param pasto
     * @param cliente
     * @param nClienti
     * @return le previsioni nelle date selezionate
     */
    public NavigableMap<Date,HashMap<String, Float>> getPrevisioni(final Pasto pasto, final Cliente cliente, final int nClienti, final Date ...dates) throws ForecastNotAvailable;
}
