package application.C;

import java.util.Date;
import java.util.HashMap;
import java.util.NavigableMap;

public interface DrawGraph {
	
	/**
     * @param start
     * @param end
     * @param ID
     * @return i consumi per disegnare il grafico
     */
	public NavigableMap<Date, HashMap<String, Float>> getGraphConsumi(final Date start, final Date end, final String ID) throws DateNotFound;
	
	/**
     * @param start
     * @param end
     * @param ID
     * @param nClienti
     * @return i consumi per disegnare il grafico
     */
	public NavigableMap<Date, HashMap<String, Float>> getGraphPrevisioni(final Date start, final Date end, final String ID, final int nClienti) throws DateNotFound;

}
