package application.C;

import application.M.Catena;

/**
 * @author marco
 *
 */
public interface WarehouseModifyUtility {
	/**
	 * Modify an information or a food waste on Tipologia
	 * 
	 * @param catena    the container of typology
	 * @param idTipo    the typology to be modified
	 * @param key       the info to be modified
	 * @param value     of the info
	 * @param newIdTipo the new name of typology
	 * @param newIdInfo the new name of the key
	 */
	void modifyTipo(final Catena catena, final String idTipo, final String key, final String value);

	/**
	 * Modify an information or a food waste on Prodotto
	 * 
	 * @param catena       the container of typology
	 * @param idProd       the id of the product to be modified
	 * @param idScarto     the waste of the product
	 * @param valoreScarto the value of waste
	 * @param key          the name of the info
	 * @param value        the name of the info
	 * @param newId        the new name of product
	 * @param newIdInfo    the new name of the key
	 * @param newIdScarto  the new name of a waste
	 */
	void modifyProdotto(final Catena catena, final String idProd, final String idScarto, final String valoreScarto,
			final String key, final String value);

	/**
	 * Modify an information or a food waste on ProdConcreto
	 * 
	 * @param catena       the container of typology
	 * @param idProdCon    the id of the product to be modified
	 * @param idScarto     the waste of the product
	 * @param valoreScarto the value of waste
	 * @param key          the name of the info
	 * @param value        the value of the info
	 * @param newId        the new name of product
	 * @param newIdInfo    the new name of the key
	 * @param newIdScarto  the new name of a waste
	 */
	void modifyProdConcreto(final Catena catena, final String idProdCon, final String idScarto, final String valoreScarto,
			final String key, final String value);

	/**
	 * Modify an information or a food waste on ProdFornito
	 * 
	 * @param catena       the container of the product
	 * @param idProdFor    the id of the product to be modified
	 * @param idProdCon    the "father" of the product
	 * @param idScarto     the waste of the product
	 * @param valoreScarto the value of waste
	 * @param key          the name of the info
	 * @param value        the value of the info
	 * @param idForn       the name of the supplier
	 * @param newId        the new name of product
	 * @param newIdInfo    the new name of the key
	 * @param newIdScarto  the new name of a waste
	 */
	void modifyProdFornito(final Catena catena, final String idProdFor, final String idScarto, final String valoreScarto,
			final String key, final String value, final String iForn);

	/**
	 * Modify an information on the Dispensa
	 * 
	 * @param catena     the container of the product
	 * @param idTipo     the typology to be added
	 * @param idHotel    the name of the hotel
	 * @param idDispensa the name of the dispensa
	 */
	void modifyTipoDispensa(final Catena catena, final String idTipo, final String idHotel, final String idDispensa, final String newId);

	/**
	 * Modify an information on the Fornitore
	 * 
	 * @param catena      the container of the product
	 * @param idFornitore the name of the supplier
	 * @param idProdotto  the name of product to be modified
	 * @param prezzo      the new price of the product
	 */
	void modifyFornitore(final Catena catena, final String idFornitore, final String idProdotto, final String prezzo, final String newId);

	/**
	 * Modiify an information on the Hotel
	 * 
	 * @param catena  the container of the product
	 * @param idHotel the name of the hotel
	 * @param newInfo the new information to be added
	 * @param newId   the new name of the hotel
	 */
	void modifyHotel(final Catena catena, final String idHotel, final String newInfo, final String newId);
	
	/**
	 * Add the quantity of product to the storage
	 * 
	 * @param catena     the container of the product
	 * @param idHotel    the name of the hotel
	 * @param idDispensa the name of the dispensa
	 * @param idProdForn the name of the product to be added
	 * @param quantita   the quantity of the prodcut
	 */
	void load(final Catena catena, final String idHotel, final String idDispensa, final String idProdForn,
			final String quantita);


	/**
	 * Remove the quantity of product to the storage
	 * 
	 * @param catena     the container of the product
	 * @param idHotel    the name of the hotel
	 * @param idDispensa the name of the dispensa
	 * @param idProdForn the name of the product to be removed
	 * @param quantita   the quantity of the prodcut
	 */
	void dump(final Catena catena, final String idHotel, final String idDispensa, final String idProdForn,
			final String quantita);

	/**
	 * Change the name of the typology
	 * 
	 * @param catena the container of the product
	 * @param id     the name of the typology
	 * @param newId  the new of the typology
	 */
	void changeName(final Catena catena, final String id, final String newId);
	
	/**
	 * Change the id of an information
	 * 
	 * @param catena    the container of the product
	 * @param id        the name of the typology
	 * @param idInfo    the name of the information
	 * @param newIdInfo the new name of the information
	 */
	void changeidInfo(final Catena catena, final String id, final String idInfo, final String newIdInfo);
	
	/**
	 * Change the id of a waste
	 * 
	 * @param catena      the container of the product
	 * @param id          the name of the product
	 * @param idScarto    the name of a waste
	 * @param newIdScarto the new name of a waste
	 */
	void changeidScarto(final Catena catena, final String id, final String idScarto, final String newIdScarto);
	
}
