package application.C;

import java.util.Date;
import java.util.Map;

import application.M.Catena;

/**
 * @author marco
 *
 */
public interface StoricFilter {

    /**
     * @param catena the container of typology
     * @param idhotel the name of the hotel
     * @param idDisp the name of the dispensa
     * @param dateStart the search start date
     * @param dateEnd the search end date 
     * @return new Map<Date,Map<String, Float>>
     */
    Map<Date,Map<String, Float>> searchload (final Catena catena, final String idhotel, final String idDisp, final Date dateStart, final Date dateEnd);
    
    
    /**
     * @param catena the container of typology
     * @param idhotel the name of the hotel
     * @param idDisp the name of the dispensa
     * @param dateStart the search start date
     * @param dateEnd the search end date 
     * @return new Map<Date, Map<String, Float>>
     */
    Map<Date,Map<String, Float>> searchdump (final Catena catena, final String idhotel, final String idDisp, final Date dateStart, final Date dateEnd);
    
}
