package application.M;

import java.io.Serializable;

public class Utente implements Serializable{
	
	private int grado;
	private String username;
	private char[] password;
	
	
	
	public Utente(String username, char[] password) {
		
		this.grado = 3;
		this.username = username;
		this.password = password;
	}
	public int getGrado() {
		return grado;
	}
	public void setGrado(int grado) {
		this.grado = grado;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public char[] getPassword() {
		return password;
	}
	public void setPassword(char[] password) {
		this.password = password;
	}

}
