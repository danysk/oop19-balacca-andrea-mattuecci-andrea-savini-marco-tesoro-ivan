package application.M;

import java.util.*;
import java.io.*;

// TODO: Auto-generated Javadoc
/**
 * The Class Fornitore.
 */
public class Fornitore implements Serializable{

    /** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2028564397034424723L;
	
	/** The id. */
	private String ID;
    
    /** The prodotti. */
    private HashMap<String,Float> prodotti; //IDProd, prezzo
    
    /** The migliori prodotti. */
    private HashMap<String,Float> miglioriProdotti; //IDProd, prezzoEffettivo -> conterrà i prodotti il cui prezzoEffettio risulta il migliore
    
    /**
     * Instantiates a new fornitore.
     *
     * @param ID del fornitore
     */
    public Fornitore(final String ID) {
        this.ID = ID;
        this.prodotti = new HashMap<String, Float>();
        this.miglioriProdotti = new HashMap<String, Float>();
    }
    
    //utility di aggiunta---------------------------------------
    
    /**
     * Aggiungi prodotti.
     *
     * @param cont la mappa di id prodotto, quantità da aggiungere
     * @return the optional vuoto se ok, con la mappa di contenuti non ammessi altrimenti
     */
    public Optional<HashMap<String,Float>> aggiungiProdotti(final HashMap<String,Float> prod) {
        
        HashMap<String,Float> prodNonAmmessi = new HashMap<>();
        for(final var c : prod.keySet()) {
            if(!prodotti.containsKey(c)) { //se il prodotto non è presente
                prodotti.put(c, (prod.get(c))); //lo inserisco
            }
            else {
                prodNonAmmessi.put(c, prod.get(c)); //altrimenti, creo la mappa da restituire per indicare quali prodotti non sono ammessi
            }
        }
        
        if(!prodNonAmmessi.isEmpty()) { //se qualche prodotto non è ammesso
            return Optional.of(prodNonAmmessi); //restituisco la mappa coi prodotti non ammessi
        }
        
        return Optional.empty(); //altrimenti restituisco un opzionale vuoto per indicare che tutto è ok
    }
    
    /**
     * Aggiungi migliori prodotti.
     *
     * @param prod la mappa di id prodotto, quantità da aggiungere
     * @return the optional vuoto se ok, con la mappa di contenuti non ammessi altrimenti
     */
    public Optional<HashMap<String,Float>> aggiungiMiglioriProdotti(final HashMap<String,Float> prod) {
        
        HashMap<String,Float> prodNonAmmessi = new HashMap<>();
        for(final var c : prod.keySet()) {
            if(!miglioriProdotti.containsKey(c)) { //se il prodotto non è presente
                miglioriProdotti.put(c, (prod.get(c))); //lo inserisco
            }
            else {
                prodNonAmmessi.put(c, prod.get(c)); //altrimenti, creo la mappa da restituire per indicare quali prodotti non sono ammessi
            }
        }
        
        if(!prodNonAmmessi.isEmpty()) { //se qualche prodotto non è ammesso
            return Optional.of(prodNonAmmessi); //restituisco la mappa coi prodotti non ammessi
        }
        
        return Optional.empty(); //altrimenti restituisco un opzionale vuoto per indicare che tutto è ok
    }
    
    //utility di modifica---------------------------------------

    /**
     * Modifica prodotti.
     *
     * @param prod la mappa di id prodotto, quantità da modificare
     * @return the optional vuoto se ok, con la mappa di contenuti non ammessi altrimenti
     */
    public Optional<HashMap<String,Float>> modificaProdotti(final HashMap<String,Float> prod) {
        
        HashMap<String,Float> prodNonAmmessi = new HashMap<>();
        for(final var c : prod.keySet()) {
            if(prodotti.containsKey(c)) { //se il prodotto è presente
                prodotti.put(c, (prod.get(c))); //lo modifico
            }
            else {
                prodNonAmmessi.put(c, prod.get(c)); //altrimenti, creo la mappa da restituire per indicare quali prodotti non sono ammessi
            }
        }
        
        if(!prodNonAmmessi.isEmpty()) { //se qualche prodotto non è ammesso
            return Optional.of(prodNonAmmessi); //restituisco la mappa coi prodotti non ammessi
        }
        
        return Optional.empty(); //altrimenti restituisco un opzionale vuoto per indicare che tutto è ok
    }
    
    /**
     * Modifica migliori prodotti.
     *
     * @param prod la mappa di id prodotto, quantità da modificare
     * @return the optional vuoto se ok, con la mappa di contenuti non ammessi altrimenti
     */
    public Optional<HashMap<String,Float>> modificaMiglioriProdotti(final HashMap<String,Float> prod) {
        
        HashMap<String,Float> prodNonAmmessi = new HashMap<>();
        for(final var c : prod.keySet()) {
            if(miglioriProdotti.containsKey(c)) { //se il prodotto è presente
                miglioriProdotti.put(c, (prod.get(c))); //lo inserisco
            }
            else {
                prodNonAmmessi.put(c, prod.get(c)); //altrimenti, creo la mappa da restituire per indicare quali prodotti non sono ammessi
            }
        }
        
        if(!prodNonAmmessi.isEmpty()) { //se qualche prodotto non è ammesso
            return Optional.of(prodNonAmmessi); //restituisco la mappa coi prodotti non ammessi
        }
        
        return Optional.empty(); //altrimenti restituisco un opzionale vuoto per indicare che tutto è ok
    }

    //utility di rimozione---------------------------------------
    
    /**
     * Rimuovi prodotti.
     *
     * @param prod la arraylist di id prodotto, quantità da rimuovere
     * @return the optional vuoto se ok, con la mappa di contenuti non ammessi altrimenti
     */
    public Optional<ArrayList<String>> rimuoviProdotti(final ArrayList<String> prod) { //vuole in pasto una lista di IDProd
        
        ArrayList<String> prodNonAmmessi = new ArrayList<>();
        for(final var c : prod) {
            if(miglioriProdotti.containsKey(c)) { //se il prodotto è presente
                miglioriProdotti.remove(c); //lo rimuovo
            }
            else {
                prodNonAmmessi.add(c); //altrimenti, creo la lista da restituire per indicare quali prodotti non sono ammessi
            }
        }
        
        if(!prodNonAmmessi.isEmpty()) { //se qualche prodotto non è ammesso
            return Optional.of(prodNonAmmessi); //restituisco la lista coi prodotti non ammessi
        }
        
        return Optional.empty(); //altrimenti restituisco un opzionale vuoto per indicare che tutto è ok
    }
    
    /**
     * Rimuovi migliori prodotti.
     *
     * @param prod la mappa di id prodotto, quantità da rimuovere
     * @return the optional vuoto se ok, con la mappa di contenuti non ammessi altrimenti
     */
    public Optional<ArrayList<String>> rimuoviMiglioriProdotti(final ArrayList<String> prod) {
        
        ArrayList<String> prodNonAmmessi = new ArrayList<>();
        for(final var c : prod) {
            if(miglioriProdotti.containsKey(c)) { //se il prodotto è presente
                miglioriProdotti.remove(c); //lo rimuovo
            }
            else {
                prodNonAmmessi.add(c); //altrimenti, creo la lista da restituire per indicare quali prodotti non sono ammessi
            }
        }
        
        if(!prodNonAmmessi.isEmpty()) { //se qualche prodotto non è ammesso
            return Optional.of(prodNonAmmessi); //restituisco la lista coi prodotti non ammessi
        }
        
        return Optional.empty(); //altrimenti restituisco un opzionale vuoto per indicare che tutto è ok
    }
    
    /**
     * Gets the id.
     *
     * @return the id
     */
    public String getID() {
        return ID;
    }
    
    /**
     * Sets the id.
     *
     * @param iD the new id
     */
    public void setID(final String iD) {
        ID = iD;
    }
    
    /**
     * Gets the prodotti.
     *
     * @param cat la catena in cui è la dispensa
     * @return the prodotti, prezzo all'interno di una hashmap
     */
    //date le numerose ed evenutali modifiche riguardanti prodotti e relativi prezzi e relativi fornitori, aggiorno di volta in volta
    public HashMap<String,Float> getProdotti(final Catena cat) {
    	HashMap<String,Float> tmpprods = new HashMap<String, Float>();
    	ProdFornito tmp;
    	for(final var p : cat.getInventario()) {
    		if(p instanceof ProdFornito) { //se è un prodfornito
    			tmp = (ProdFornito) p;
    			if(tmp.getIDFornitore().equals(this.ID)) { //controllo il suo fornitore
    				tmpprods.put(tmp.getID(), tmp.getPrezzo()); //se matcha lo considero
    			}
    		}
    	}
    	this.prodotti = tmpprods;
        return prodotti;
    }
    
    /**
     * Sets the prodotti.
     *
     * @param prodotti the prodotti
     */
    public void setProdotti(final HashMap<String,Float> prodotti) {
        this.prodotti = prodotti;
    }
    
    /**
     * Gets the migliori prodotti.
     *
     * @param cat la catena in cui è la dispensa
     * @return the migliori prodotti, prezzo effettivo migliore all'interno di una hashmap
     */
    public HashMap<String, Float> getMiglioriProdotti(final Catena cat) {
    	
    	HashMap<String,Float> tmpprods = new HashMap<String, Float>();
    	ProdConcreto tmp;
    	for(final var p : cat.getInventario()) {
    		if(p instanceof ProdConcreto) { //se è un prodconcreto
    			tmp = (ProdConcreto) p;
    			if(tmp.getIDMigliorFornitore(cat).equals(this.ID)) { //controllo il suo miglior fornitore
    				tmpprods.put(tmp.getID(), tmp.getPrezzoEffettivoMigliore(cat)); //se matcha lo considero
    			}
    		}
    	}
    	
    	this.miglioriProdotti = tmpprods;
    	
        return miglioriProdotti;
    }
    
    /**
     * Sets the migliori prodotti.
     *
     * @param miglioriProdotti the migliori prodotti
     */
    public void setMiglioriProdotti(final HashMap<String,Float> miglioriProdotti) {
        this.miglioriProdotti = miglioriProdotti;
    }
}
