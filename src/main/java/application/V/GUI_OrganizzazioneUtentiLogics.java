package application.V;

import java.util.HashMap;

import javax.swing.JPanel;

import application.M.Utente;

public interface GUI_OrganizzazioneUtentiLogics {
	  /**
     * @return users map
     */
    public HashMap<String, Utente> getMappa() ;

	/**
	 * @param mappa
	 * 
	 * set user map
	 */
	public void setMappa(HashMap<String, Utente> mappa);

	/**
	 * Read users saved
	 */
	public void leggiElencoConMAPPA();
    
    /**
     * @param tx
     * @param p
     * 
     * Save a new user
     */
    public void scriviNuovoUtenteConMAPPA(String tx, char[] p);
    
    /**
     * Refresh users list
     */
    public void writeSulFileConMAPPA();
    
    /**
     * @param panel
     * 
     * Writes users on the panel
     */
    public void writePannelloConMAPPA(JPanel panel);
    
    /**
     * Remove user selected
     */
    public void removeSelectedCheckboxConMAPPA();
    
    /**
     * Upgrade users 
     */
    public void promozioneConMAPPA();
    
    /**
     * Downgrade users 
     */
    public void declassamentoConMAPPA();
    
    /**
     * @return true if only one user is selected
     */
    public boolean soloUnoSelezionato();
    
    /**
     * @return the username of the selected user
     */
    public String getFirstSelected();
    
    /**
     * @param tx
     * @return true if Username is already presente, false in other cases
     */
    public boolean UtenteGiaPresente(String tx);

    /**
     * @return true if admin is already presente
     */
    public boolean AdminGiaPresente();
    
    /**
     * @param tx
     * @param p
     * 
     * Create new user
     */
    public void CreaAdmin(String tx, char[] p);
}
