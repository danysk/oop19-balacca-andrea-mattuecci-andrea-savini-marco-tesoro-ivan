package application.V;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.JTextPane;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.util.Date;
import java.awt.event.ActionEvent;
import com.toedter.calendar.JDayChooser;

import application.M.ProdFornito;

import com.toedter.calendar.JDateChooser;
import javax.swing.JTable;
import java.awt.Panel;
import javax.swing.JComboBox;

public class GUI_Storico extends GUI {

    private JPanel contentPane;
    private JTable table;

    /**
     * Create the frame.
     */
    public GUI_Storico() {
    	
    	JComboBox comboBox = new JComboBox();
    	JDateChooser dateChooser = new JDateChooser();
    	JDateChooser dateChooser_1 = new JDateChooser();
    	
    	
    	
        for(var x : GUI.catenaAccesso.ottieniUnAlbergo(GUI.hotelAccesso.getNome()).get().getDispense()) {			
				comboBox.addItem(x.getNome());			
		}
    	
    	table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {},new String[] {"Data","ID Prodotto Fornito", "Quantità"})
		{
			Class[] columnTypes = new Class[] {
					String.class,String.class, Float.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
			boolean[] columnEditables = new boolean[] {
				false, false
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		
		DefaultTableModel model = (DefaultTableModel)table.getModel();
		
		setMinimumSize(new Dimension(777, 647));
    	
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 777, 647);
        contentPane = new JPanel();
        contentPane.setBackground(new Color(176, 224, 230));
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);
        
        JLabel lblStorico = new JLabel("Storico");
        lblStorico.setHorizontalAlignment(SwingConstants.CENTER);
        lblStorico.setFont(new Font("Lucida Grande", Font.PLAIN, 40));
        contentPane.add(lblStorico, BorderLayout.NORTH);
        
        JPanel panel_1_1 = new JPanel();
        panel_1_1.setBackground(new Color(255, 222, 173));
        contentPane.add(panel_1_1, BorderLayout.SOUTH);
        
        JButton btnSalva_1_1 = new JButton("Indietro");
        btnSalva_1_1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                
                save(getX(), getY(), getWidth(), getHeight());
                
                frame = new GUI_GeneraleHotel();
                frame.setBounds(getX(), getY(), getWidth(), getHeight());
               
                frame.setVisible(true);
                setVisible(false);
                
            }
        });
        btnSalva_1_1.setPreferredSize(new Dimension(117, 50));
        panel_1_1.add(btnSalva_1_1);
        
        
        
        JButton btnSalva_1_1_1 = new JButton("Cerca carico");
        btnSalva_1_1_1.addActionListener(new ActionListener() {
        	/**
        	 * Search carico in the selected dates
        	 */
        	public void actionPerformed(ActionEvent e) {
        		
        		try {
					
				
	        		Date data1 = dateChooser.getDate();
	        		Date data2 = dateChooser_1.getDate();
	        		String dispensa = comboBox.getSelectedItem().toString();
	        		
	        		model.setRowCount(0);
	        		
	        		try {
	        			
	        			for(var x : s.searchload(GUI.catenaAccesso, GUI.hotelAccesso.getNome(), dispensa, data1, data2).keySet()) {
	        				for(var y : s.searchload(GUI.catenaAccesso, GUI.hotelAccesso.getNome(), dispensa, data1, data2).get(x).keySet()) {
		        				model.addRow(new Object[] {""+x , y, s.searchload(GUI.catenaAccesso, GUI.hotelAccesso.getNome(), dispensa, data1, data2).get(x).get(y)});
	        				}
	        			}
	        			
					} catch (Exception e2) {
						JOptionPane.showMessageDialog(null, "Inserire entrambe le date", "ERRORE", JOptionPane.ERROR_MESSAGE);  
						//e2.printStackTrace();
					}
        		} catch (Exception e2) {
        			JOptionPane.showMessageDialog(null, "Errore cerca carico", "ERRORE", JOptionPane.ERROR_MESSAGE);
				}
        		
        	}
        });
        btnSalva_1_1_1.setPreferredSize(new Dimension(117, 50));
        panel_1_1.add(btnSalva_1_1_1);
        
        JButton btnSalva_1_1_1_1 = new JButton("Cerca scarico");
        btnSalva_1_1_1_1.addActionListener(new ActionListener() {
        	/**
        	 * Search scarico in the selected dates
        	 */
        	public void actionPerformed(ActionEvent e) {
        		
        		try {
					
				
	        		Date data1 = dateChooser.getDate();
	        		Date data2 = dateChooser_1.getDate();
	        		String dispensa = comboBox.getSelectedItem().toString();
	        		
	        		model.setRowCount(0);
	        		
	        		try {

	        			for(var x : s.searchdump(GUI.catenaAccesso, GUI.hotelAccesso.getNome(), dispensa, data1, data2).keySet()) {
	        				for(var y : s.searchdump(GUI.catenaAccesso, GUI.hotelAccesso.getNome(), dispensa, data1, data2).get(x).keySet()) {
		        				model.addRow(new Object[] {""+x,y, s.searchdump(GUI.catenaAccesso, GUI.hotelAccesso.getNome(), dispensa, data1, data2).get(x).get(y)});
	        				}
	        			}
	        			
					} catch (Exception e2) {
						JOptionPane.showMessageDialog(null, "Inserire entrambe le date", "ERRORE", JOptionPane.ERROR_MESSAGE);  
					}
        		} catch (Exception e2) {
        			JOptionPane.showMessageDialog(null, "Errore cerca carico", "ERRORE", JOptionPane.ERROR_MESSAGE);  
				}
        		
        	}
        });
        btnSalva_1_1_1_1.setPreferredSize(new Dimension(117, 50));
        panel_1_1.add(btnSalva_1_1_1_1);
        
        JPanel panel = new JPanel();
        contentPane.add(panel, BorderLayout.CENTER);
        panel.setLayout(new BorderLayout(0, 0));
        
        JPanel panel_1 = new JPanel();
        panel_1.setPreferredSize(new Dimension(10, 100));
        panel.add(panel_1, BorderLayout.NORTH);
        panel_1.setLayout(new GridLayout(0, 3, 0, 0));
        
        JPanel panel_2 = new JPanel();
        panel_1.add(panel_2);
        panel_2.setLayout(new BorderLayout(0, 0));
        
        JLabel lblDataInizioRicerca = new JLabel("Data inizio ricerca");
        lblDataInizioRicerca.setHorizontalAlignment(SwingConstants.CENTER);
        lblDataInizioRicerca.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
        panel_2.add(lblDataInizioRicerca, BorderLayout.NORTH);
        
        Panel panel_5 = new Panel();
        panel_2.add(panel_5, BorderLayout.CENTER);
        
       
        GroupLayout gl_panel_5 = new GroupLayout(panel_5);
        gl_panel_5.setHorizontalGroup(
        	gl_panel_5.createParallelGroup(Alignment.LEADING)
        		.addGroup(Alignment.TRAILING, gl_panel_5.createSequentialGroup()
        			.addContainerGap(45, Short.MAX_VALUE)
        			.addComponent(dateChooser, GroupLayout.PREFERRED_SIZE, 166, GroupLayout.PREFERRED_SIZE)
        			.addGap(44))
        );
        gl_panel_5.setVerticalGroup(
        	gl_panel_5.createParallelGroup(Alignment.LEADING)
        		.addGroup(gl_panel_5.createSequentialGroup()
        			.addGap(23)
        			.addComponent(dateChooser, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        			.addContainerGap(26, Short.MAX_VALUE))
        );
        panel_5.setLayout(gl_panel_5);
        
        JPanel panel_3 = new JPanel();
        panel_1.add(panel_3);
        panel_3.setLayout(new BorderLayout(0, 0));
        
        JLabel lblDataInizioRicerca_1_1 = new JLabel("Dispensa");
        lblDataInizioRicerca_1_1.setHorizontalAlignment(SwingConstants.CENTER);
        lblDataInizioRicerca_1_1.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
        panel_3.add(lblDataInizioRicerca_1_1, BorderLayout.NORTH);
        
        Panel panel_6 = new Panel();
        panel_3.add(panel_6, BorderLayout.CENTER);
        
        
        GroupLayout gl_panel_6 = new GroupLayout(panel_6);
        gl_panel_6.setHorizontalGroup(
        	gl_panel_6.createParallelGroup(Alignment.LEADING)
        		.addGroup(Alignment.TRAILING, gl_panel_6.createSequentialGroup()
        			.addGap(59)
        			.addComponent(comboBox, 0, 148, Short.MAX_VALUE)
        			.addGap(48))
        );
        gl_panel_6.setVerticalGroup(
        	gl_panel_6.createParallelGroup(Alignment.LEADING)
        		.addGroup(gl_panel_6.createSequentialGroup()
        			.addGap(25)
        			.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        			.addContainerGap(23, Short.MAX_VALUE))
        );
        panel_6.setLayout(gl_panel_6);
        
        JPanel panel_4 = new JPanel();
        panel_1.add(panel_4);
        panel_4.setLayout(new BorderLayout(0, 0));
        
        JLabel lblDataInizioRicerca_1 = new JLabel("Data fine ricerca");
        lblDataInizioRicerca_1.setHorizontalAlignment(SwingConstants.CENTER);
        lblDataInizioRicerca_1.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
        panel_4.add(lblDataInizioRicerca_1, BorderLayout.NORTH);
        
        Panel panel_7 = new Panel();
        panel_4.add(panel_7, BorderLayout.CENTER);
        
        
        GroupLayout gl_panel_7 = new GroupLayout(panel_7);
        gl_panel_7.setHorizontalGroup(
        	gl_panel_7.createParallelGroup(Alignment.LEADING)
        		.addGroup(Alignment.TRAILING, gl_panel_7.createSequentialGroup()
        			.addContainerGap(43, Short.MAX_VALUE)
        			.addComponent(dateChooser_1, GroupLayout.PREFERRED_SIZE, 169, GroupLayout.PREFERRED_SIZE)
        			.addGap(43))
        );
        gl_panel_7.setVerticalGroup(
        	gl_panel_7.createParallelGroup(Alignment.TRAILING)
        		.addGroup(Alignment.LEADING, gl_panel_7.createSequentialGroup()
        			.addGap(25)
        			.addComponent(dateChooser_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        			.addContainerGap(24, Short.MAX_VALUE))
        );
        panel_7.setLayout(gl_panel_7);
        
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        panel.add(scrollPane, BorderLayout.CENTER);
        
       
        scrollPane.setViewportView(table);
    }
}
